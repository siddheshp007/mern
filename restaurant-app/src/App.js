import './App.css';
import {
  BrowserRouter as Router,
  Routes, Route,
  Link
} from 'react-router-dom';
import Home from './components/Home';
import RestaurantCreate from './components/RestaurantCreate';
import RestaurantDetails from './components/RestaurantDetails';
import RestaurantList from './components/RestaurantList';
import RestaurantSearch from './components/RestaurantSearch';
import RestaurantUpdate from './components/RestaurantUpdate';
import { Navbar, Nav, Container } from 'react-bootstrap'

function App() {
  return (
    <div className="App">
      <Router>

        <Navbar bg="light" expand="lg">
          <Container>
            <Navbar.Brand href="#home">Restaurant</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto">
                <Nav.Link href="/">Home</Nav.Link>
                <Nav.Link href="/list">List</Nav.Link>
                <Nav.Link href="/create">Create</Nav.Link>
                <Nav.Link href="/update">update</Nav.Link>
                <Nav.Link href="/detail">Details</Nav.Link>
                <Nav.Link href="/search">Search</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>

        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route path="/list" element={<RestaurantList />} />
          <Route path="/create" element={<RestaurantCreate />} />
          <Route path="/update" element={<RestaurantUpdate />} />
          <Route path="/detail" element={<RestaurantDetails />} />
          <Route path="/search" element={<RestaurantSearch />} />
        </Routes>


      </Router>
    </div>
  );
}

export default App;
