import React from 'react'
import { Table } from 'react-bootstrap'
class RestaurantList extends React.Component {
    constructor() {
        super()
        this.state = {
            list: null
        }
    }

    //Call as DOM element
    componentDidMount() {
        fetch("http://localhost:3000/restaurants").then((response) => {
            response.json().then((result) => {
                this.setState({ list: result })
            })
        })
    }

    render() {

        return (
            <div>
                <h1> Restaurant List </h1>
                {
                    this.state.list ?
                        <div>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Location</th>
                                        <th>Rating</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.list.map((item, i) =>
                                            <tr>
                                                <td>{i}</td>
                                                <td>{item.title}</td>
                                                <td>{item.location}</td>
                                                <td>{item.rating}</td>
                                                <td>{item.email}</td>
                                                <td></td>
                                            </tr>
                                        )
                                    }
                                </tbody>
                            </Table>
                        </div>
                        : <p>Please wait..</p>

                }

            </div>
        )
    }
}
export default RestaurantList;